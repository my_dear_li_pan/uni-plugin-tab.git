# 一、概要
1. 我在做项目的过程中用到了tab切换，本想着去插件市场找一个直接现用，后面发现找到的tab切换并不是我想要的那种使用方式，于是我结合了iview中Tab切换的方式打造了这个插件，可能还有很多还需要完善的地方，再接再厉！
2. 该组件适用于小程序，app-vue需要用的话，我暂时没有测试，最好是做进一步的优化，否则可能会有性能的问题。
3. 该插件同时兼容了VUE2和VUE3编译。
4. 如果发现组件有BUG或者不完善可以留言交流。
5. git地址：https://gitee.com/my_dear_li_pan/uni-plugin-tab.git
6. uniapp插件市场：https://ext.dcloud.net.cn/plugin?id=6177

# 二、注意事项
1. 该插件使用的预编译，需要自行安装scss/sass插件。
2. 该插件同时兼容了VUE2和VUE3，所以hbuilderX 需要更新到3.2.2及其以上。

# 三、支持平台
微信小程序、百度小程序、支付宝小程序、字节小程序、QQ小程序、H5

# 四、使用说明
```
<template>
	<view>
		<tab index="0" :animation="true" tabPadding="40" @changeIndex="changeIndex">
			<tab-pane label="标签一">
				<view style="height: 100px;">内容1</view>
				<view style="height: 100px;">内容1</view>
				<view style="height: 100px;">内容1</view>
				<view style="height: 100px;">内容1</view>
				<view style="height: 100px;">内容1</view>
				<view style="height: 100px;">内容1</view>
				<view style="height: 100px;">内容1</view>
				<view style="height: 100px;">内容1</view>
				<view style="height: 100px;">内容1</view>
				<view style="height: 100px;">内容1</view>
			</tab-pane>
			<tab-pane label="标签二">内容2</tab-pane>
			<tab-pane label="标签三3">内容3</tab-pane>
			<tab-pane label="标签四44">内容4</tab-pane>
			<tab-pane label="标签五555">内容5</tab-pane>
			<tab-pane label="标签六6666">内容6</tab-pane>
			<tab-pane label="标签七77777">内容7</tab-pane>
		</tab>
	</view>
</template>
<script>
	import tab from '@/components/lipan-tabs/lipan-tabs.vue';
	import tabPane from '@/components/lipan-tabs/lipan-tab-pane.vue';
	export default {
		components: {
			tab,
			tabPane
		},
		// #ifdef VUE2
		methods: {
			changeIndex(index) {
				console.log('改变了index:', index)
			}
		},
		// #endif
		// #ifdef VUE3
		setup() {
			function changeIndex(index) {
				console.log('改变了index:', index)
			}
			return {
				changeIndex
			}
		}
		// #endif
	}
</script>
<style scoped lang="scss">
</style>
```

# 五、属性说明
名称|类型|是否必填|默认值|可选值|说明
-|-|-|-|-|-|-|
index|[Number,String]|否|0|0到(tab总数-1)|显示某一项tab的下标
animation|[Boolean]|否|true|true/false|切换动画
tabPadding|[Number,String]|否|20|根据自己的需求|tab选项的左右padding值

# 六、方法说明
名称|说明
-|-|
changeIndex|切换回调，返回index下标

### 有任何问题欢迎大家在评论区讨论。开源不易，可以收藏、点个赞哟~~~




